package ejerciciosif;

import java.util.Scanner;

public class EjerciciosIf {
    
    // Imprime el mensaje y salta una línea
    static void imprimirMsjL(String sMsj) {
        System.out.println(sMsj);
    }
    
    // Imprime el mensaje sin saltar una línea
    static void imprimirMsjSinL(String sMsj) {
        System.out.print(sMsj);
    }

    // Imprime un separador
    static void separador() {
        imprimirMsjL("---------------------------------------------------------------------------------------");
    }
    
    // Imprime información del programa y personal
    static void infoP() {
        imprimirMsjL("Ejercicios If");
        imprimirMsjL("Uc Vazquez D. Carolina, 57618");
        separador();
    }
    
    // Menú con opciones posibles a realizar
    static void menu() {
        Scanner tec = new Scanner (System.in);
        imprimirMsjL("Elija la opción que desee entre las siguientes:\n");
        imprimirMsjL("1.- ¿Cuál número es el mayor y cuál número es el menor? (Ej. 01)");
        imprimirMsjL("2.- ¿El tercer número está más cerca del primer número o del segundo número? (Ej. 10)");
        imprimirMsjL("3.- Finalizar programa");
        imprimirMsjSinL("\n¿Qué número de opción eligió? ");
        int iOpcion = tec.nextInt();
        submenu(iOpcion);
    }
    
    // Submenú que realiza la opción deseada
    static void submenu(int iOpcion) {
        separador();
        if (iOpcion == 1){
            imprimirMsjL("-¿Cuál número es el mayor y cuál número es el menor?-");            
            ejercicioNum01();
            separador();
        }
        if (iOpcion == 2){
            imprimirMsjL("-¿El tercer número ingresado está más cerca del primer número o del segundo número?-");
            ejercicioNum10();
            separador();
        }
        if (iOpcion == 3){
            imprimirMsjL("¡Vuelva pronto!");
            separador();
        }
    }
    
    // Ejercicio 01 - Número mayor y número menor
    static void ejercicioNum01() {
        Scanner tec = new Scanner (System.in); 
        imprimirMsjSinL("Ingrese el primer número: ");
        int iNum1 = tec.nextInt();
        imprimirMsjSinL("Ingrese el segundo número: ");
        int iNum2 = tec.nextInt();
        separador();
        if (iNum1 > iNum2) {
            System.out.println("El número mayor es: " + iNum1 + "\nY el número menor es: " + iNum2);
        }
        else if (iNum2 > iNum1) {
            System.out.println("El número mayor es: " + iNum2 + "\nY el número menor es: " + iNum1);
        }
        else {
            imprimirMsjL("Los números son iguales");
        }
    }
    
    // Ejercicio 10 - Determinar si el tercer número está más cerca del primero o del segundo
    static void ejercicioNum10() {
        Scanner tec = new Scanner (System.in);
        imprimirMsjSinL("Ingrese el primer número: ");
        int iNum1 = tec.nextInt();
        imprimirMsjSinL("Ingrese el segundo número: ");
        int iNum2 = tec.nextInt();
        imprimirMsjSinL("Ingrese el tercer número: ");
        int iNum3 = tec.nextInt();
        int iResult1 = Math.abs(iNum1 - iNum3);
        int iResult2 = Math.abs(iNum2 - iNum3);
        separador();
        if (iResult1 > iResult2) {
            imprimirMsjL("¡El tercer número está más cerca del segundo número!");
            System.out.println(iNum3 + " está más cerca de " + iNum2 + " que de " + iNum1);
        }
        else if (iResult2 > iResult1) {
            imprimirMsjL("¡El tercer número está más cerca del primer número!");
            System.out.println(iNum3 + " está más cerca de " + iNum1 + " que de " + iNum2);
        }
        else {
            imprimirMsjL("¡El tercer número está igual de cerca del primer y del segundo número!");
            System.out.println(iNum3 + " está igual de cerca de " + iNum2 + " que de " + iNum1);
        }
    }
    
    // Run
    public static void main(String[] args) {
        infoP();
        menu();
        infoP();
    }
    
}